package com.example.demo;

import com.example.demo.service.RandomService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

    @Autowired
    private TestRestTemplate testRestTemplate;
    @Autowired
    private RandomService randomService;


    @Test
    public void contextLoads() {
    }

    @Test
    public void homeResponse() {
        String body = this.testRestTemplate.getForObject("/", String.class);
        assertThat(body).isEqualTo("home");
    }

    @Test
    public void sayHi() {
        String a = randomService.sayHi();
        Assert.assertSame("hi", a);
    }

    @Test
    public void failure() {
        fail("test failure!!!");
    }
}
